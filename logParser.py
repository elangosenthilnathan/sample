import re
import argparse
import sys
import os.path
import time
import datetime
import prometheus_client
from prometheus_client import Histogram

######################################################
##### GLOBAL VARIABLES ###########
######################################################
logFile = ""

parser = argparse.ArgumentParser(description='Input Params')
parser.add_argument('LOGFILE', metavar='L', nargs='?', default=None, help='Project Log File for parsing')
args = parser.parse_args()
if args.LOGFILE:
    logFile = args.LOGFILE.upper()

#################################
#
#  Main Method
#
#################################
def main():

    startTime = time.time()

    prometheus_client.start_http_server(9998)
    if not args.LOGFILE:
        print("No logfile provided. Exiting code")
        sys.exit(0)
    elif not os.path.exists(logFile):
        print("Logfile provided is not found. Exiting code")
        sys.exit(0)

    promethesTxHistogram = Histogram("tx_values","TX Values",buckets=[500, 1000, 5000, 9000])
    promethesRxHistogram = Histogram("rx_values","RX Values",buckets=[500, 1000, 5000, 9000])

    logFileIdentifier = logFile
    regexStrTx = '"Tx":\d+'
    regexStrRx = '"Rx":\d+'


    with open(logFileIdentifier, "r") as file:
        matchList = []
        for eachLine in file:
            for match in re.finditer(regexStrTx, eachLine):
                #print("Tx %s" % (match.group().split(':')[1]))
                TxContent = int(match.group().split(':')[1])
                promethesTxHistogram.observe(TxContent)
            for match in re.finditer(regexStrRx, eachLine):
                #print("Rx %s" % (match.group().split(':')[1]))
                rxContent = int(match.group().split(':')[1])
                promethesTxHistogram.observe(rxContent)



    file.close()
    print("Execution Time of logParser is %s Seconds. Executed on %s" % (time.time() - startTime, datetime.datetime.now().isoformat(' ')))

if __name__ == "__main__":
    main()
