import logging
import sys
import time
import os
import datetime
from datetime import timedelta
import random

#################################
#
#   Logger Configuration
#
#################################
# NOTE: To Debug Failures, change the logging level to DEBUG eg. level=logging.INFO
logname = 'Logs/' + "logGenerator_" + time.strftime("%Y%m%d") + ".log"
logging.basicConfig(filename=logname,filemode='a',format='{"level":'"'%(levelname)s'"',"ts":%(message)s,"conn_id":%(connIDVar)s,"state":%(stateVar)s,"Tx":%(TxVar)s,"Rx":%(RxVar)s}', datefmt="%Y-%m-%d %H:%M:%S", level=logging.DEBUG)
logger = logging.getLogger()

#################################
#
#  Main Method
#
#################################
def main():
    myPreferredDateOrig = datetime.datetime(2021, 4, 15)
    prefDateInt = int(myPreferredDateOrig.timestamp())
    prefDateIntOrig = prefDateInt

    isTrue = True
    connIdIncrement = 5406983 #some random number
    stateInfo = "Closed"

    startTime = time.time()

    while isTrue:

        prefDateInt += 1
        connIdIncrement += 1
        #myPreferredDateMod = myPreferredDateMod + timedelta(seconds=1)
        logger.debug(prefDateInt, extra={"connIDVar": connIdIncrement, "stateVar": '"' + stateInfo + '"', "RxVar": random.randint(0,10000), "TxVar": random.randint(0,10000)})
        if (prefDateInt - prefDateIntOrig)  > 86400: # 24 hours has 86400 seconds
            isTrue = False

    print("Execution Time of logGenerator is %s Seconds. Executed on %s" % (time.time() - startTime, datetime.datetime.now().isoformat(' ')))

if __name__ == "__main__":
    main()
