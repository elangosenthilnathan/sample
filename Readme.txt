1. Take this line of code as an example

{"level":"debug","ts":1578025817,"conn_id":5406983,"satte":"closed","Tx":404,"Rx":879}

2. Write logs generator that will generate logs of the given format for 24 hours on April 15th, 2021 with 1 record per second, increment conn_id by 1 for each record, with random values for TX and TX from 0-10000

3. Write simple python program that will parse log file and export into file two metrics in prometheus format:
3.1 Histogram of TX values with buckets:
<500
<1000
<5000
<9000
<inf

3.2 Histogram of RX values with buckets:
<500
<1000
<5000
<9000
<Inf

ts - unix time. Please also measure the execution time for generation and parsing (Separately)

========================

